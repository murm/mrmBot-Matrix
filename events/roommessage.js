import database from "../utils/database.js";
import { log, error as _error, logger } from "../utils/logger.js";
import { prefixCache, aliases, disabledCache, disabledCmdCache, commands } from "../utils/collections.js";
import parseCommand from "../utils/parseCommand.js";
import { clean } from "../utils/misc.js";
// import { upload } from "../utils/tempimages.js";

let mentionRegex;

// run when someone sends a message
export default async function (matrixClient, event, room, toStartOfTimeline) {
    // console.log(matrixClient)
    if (event.getType() == "m.room.message") {
        if (toStartOfTimeline) {
            return; // don't act on paginated results
        }
        if (event.event.sender == process.env.MATRIX_USERNAME) return;
        // console.log(event.event);
        let text;
        text = event.event.content.body;
        // if a reply, strip the reply from the formatting
        text = text.replace(/.*\n\n/g, "")
        if (text.startsWith(process.env.PREFIX)) {
            text = text.substring(process.env.PREFIX.length).trim();
        } else {
            return;
        }

        // separate commands and args
        const preArgs = text.split(/\s+/g);
        const command = preArgs.shift().toLowerCase();
        const aliased = aliases.get(command);

        const cmd = commands.get(aliased ?? command);
        if (!cmd) return;

        // command time :peachtime:
        log("log", `${event.sender.name} (${event.event.sender}) ran command ${command}`);
        const reference = {
            messageReference: {
              channelID: event.event.room_id,
              messageID: event.event.event_id,
              guildID: undefined,
              failIfNotExists: false
            },
            allowedMentions: {
              repliedUser: false
            }
        };
        try {
            // parse args
            const parsed = parseCommand(preArgs);
            if (database) {
                await database.addCount(aliases.get(command) ?? command);
            }
            const startTime = new Date();
            // eslint-disable-next-line no-unused-vars
            const commandClass = new cmd(matrixClient, { type: "classic", message: event.event, args: parsed._, content: text.replace(command, "").trim(), specialArgs: (({ _, ...o }) => o)(parsed) }); // we also provide the message content as a parameter for cases where we need more accuracy
            const result = await commandClass.run();
            const endTime = new Date();
            if ((endTime - startTime) >= 180000) reference.allowedMentions.repliedUser = true;
            if (typeof result === "string") {
                const content = {
                    body: result,
                    msgtype: "m.text",
                };
                matrixClient.sendEvent(event.event.room_id, "m.room.message", content, "", (err, res) => {
                    console.log(err);
                });
            } else if (typeof result === "object") {
              if (result.html) {
                const content = {
                  format: "org.matrix.custom.html",
                  body: result.html,
                  formatted_body: result.html,
                  msgtype: "m.text",
                };
                matrixClient.sendEvent(event.event.room_id, "m.room.message", content, "", (err, res) => {
                    console.log(err);
                });
              }
              if (result.contents && result.name) {
                let fileSize = 52428308;
                if (result.contents.length > fileSize) {
                  if (process.env.TEMPDIR && process.env.TEMPDIR !== "") {
                    await upload(client, result, message);
                  } else {
                      const content = {
                          body: "imag too big :(",
                          msgtype: "m.text",
                      };
                      matrixClient.sendEvent(event.event.room_id, "m.room.message", content, "", (err, res) => {
                          console.log(err);
                      });
                  }
                } else {
                  const mxcUri = await matrixClient.uploadContent(result.contents);
                  // TODO: make info object get width, height, and mime from natives so i dont need to read the buffer
                  // const imgsize = sizeOf(result.contents)
                  const mime = result.type;
                  const imgsize = {
                    width: result.width,
                    height: result.height
                  }
                  await matrixClient.sendImageMessage(event.event.room_id, mxcUri.content_uri, {h: imgsize.height, w: imgsize.width, mimetype: `image/${mime}`, size: result.contents.length, thumbnail_info: {h: imgsize.height, w: imgsize.width, mimetype: `image/${mime}`, size: result.contents.length}}, result.name)
                }
              } else {
                // no-op
              }
            } else {
              // no-op
            }
        } catch (error) {
            logger.log("error", error.stack)
            try {
              const errorcontent = {
                body: `Message: ${clean(error)}\n\nStack Trace: ${clean(error.stack)}`,
                msgtype: "m.text"
              }
              matrixClient.sendEvent(event.event.room_id, "m.room.message", errorcontent, "", (err, res) => {
                console.log(err);
              });
            } catch (e) {
                _error(`While attempting to send the previous error message, another error occurred: ${e.stack || e}`);
            }
        }
        return;
    }
    return;
};