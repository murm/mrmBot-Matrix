class Command {
  success = true;
  constructor(matrixClient, options) {
    this.client = matrixClient;
    this.origOptions = options;
    this.type = options.type;
    this.args = options.args;
    this.message = options.message;
    this.channel = options.message.room_id;
    this.guild = options.message.room_id;
    this.author = options.message.sender;
    this.member = options.message.member;
    this.content = options.content;
    this.options = options.specialArgs;
    this.reference = {
      messageReference: {
        channelID: this.message.channelID,
        messageID: this.message.id,
        guildID: this.message.guildID ?? undefined,
        failIfNotExists: false
      },
      allowedMentions: {
        repliedUser: false
      }
    };
  }

  async run() {
    return "It works!";
  }

  async acknowledge() {
    if (this.type === "classic") {
      const channel = this.channel;
      await this.client.sendTyping(channel, true, 5);
    } else if (!this.interaction.acknowledged) {
      await this.interaction.defer();
    }
  }

  static init() {
    return this;
  }

  static description = "No description found";
  static aliases = [];
  static arguments = [];
  static flags = [];
  static slashAllowed = true;
  static directAllowed = true;
  static adminOnly = false;
}

export default Command;