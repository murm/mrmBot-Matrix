import ImageCommand from "../../classes/imageCommand.js";

class CitySeventeenCommand extends ImageCommand {
  static category = "image-editing"
  static description = "Welcome everyone to City 17!";

  params = {
    assetPath: "assets/images/city17.png",
    distortPath: "assets/images/city17map.png",
    compx: 132,
    compy: 366
  };

  static aliases = ["cityseventeen"];

  static noImage = "You need to provide an image/GIF!";
  static command = "scott";
}

export default CitySeventeenCommand;
