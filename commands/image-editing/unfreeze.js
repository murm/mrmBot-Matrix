import ImageCommand from "../../classes/imageCommand.js";

class UnfreezeCommand extends ImageCommand {
  static category = "image-editing"
  params = {
    loop: true
  };

  static description = "Unfreezes an image sequence";

  static requiresGIF = true;
  static noImage = "You need to provide an image/GIF to unfreeze!";
  static command = "freeze";
}

export default UnfreezeCommand;
