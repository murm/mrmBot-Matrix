import { clean, htmlescape } from "../../utils/misc.js";
import * as util from "util";
import { exec as baseExec } from "child_process";
const exec = util.promisify(baseExec);
import Command from "../../classes/command.js";

class ExecCommand extends Command {
  static category = "general"
  async run() {
    const owners = process.env.OWNER.split(",");
    if (!owners.includes(this.author)) {
      this.success = false;
      return "Only the bot owner can use exec!";
    }
    // await this.acknowledge();
    const code = this.options.cmd ?? this.args.join(" ");
    try {
      const execed = await exec(code);
      if (execed.stderr) return { html: `<pre><code>${htmlescape(await clean(execed.stderr))}</code></pre>` };
      const cleaned = await clean(execed.stdout);
      const sendString = `${cleaned}`;
      if (sendString.length >= 4000) {
        return {
          text: "The result was too large, so here it is as a file:",
          file: cleaned,
          name: "result.txt"
        };
      } else {
        return { html: "<pre><code>"+htmlescape(sendString)+"</code></pre>" };
      }
    } catch (err) {
      return `\`ERROR\` ${await clean(err)}`;
    }
  }

  static flags = [{
    name: "cmd",
    type: 3,
    description: "The command to execute",
    required: true
  }];

  static description = "Executes a shell command";
  static aliases = ["runcmd"];
  static arguments = ["[command]"];
  static adminOnly = true;
}

export default ExecCommand;