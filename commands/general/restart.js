import Command from "../../classes/command.js";

class RestartCommand extends Command {
  static category = "general"
  async run() {
    const owners = process.env.OWNER.split(",");
    if (!owners.includes(this.author)) {
      this.success = false;
      return "Only the bot owner can restart me!";
    }
    const content = {
      body: "mrmBot is restarting",
      msgtype: "m.text",
    };
    this.client.sendEvent(this.channel, "m.room.message", content, "", (err, res) => {
      console.log(err);
    });
    process.exit(1);
  }

  static description = "Restarts me";
  static aliases = ["reboot"];
  static adminOnly = true;
}

export default RestartCommand;