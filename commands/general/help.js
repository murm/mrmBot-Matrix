// import database from "../../utils/database.js";
import * as collections from "../../utils/collections.js";
import { htmlescape } from "../../utils/misc.js";
import * as help from "../../utils/help.js";
import Command from "../../classes/command.js";
// const tips = ["You can change the bot's prefix using the prefix command.", "Image commands also work with images previously posted in that channel.", "You can use the tags commands to save things for later use.", "You can visit https://esmbot.net/help.html for a web version of this command list.", "You can view a command's aliases by putting the command name after the help command (e.g. help image).", "Parameters wrapped in [] are required, while parameters wrapped in {} are optional.", "esmBot is hosted and paid for completely out-of-pocket by the main developer. If you want to support development, please consider donating! https://patreon.com/TheEssem"];

class HelpCommand extends Command {
  async run() {
    let html;
    if (this.args.length !== 0) {
      if (collections.commands.has(this.args[0].toLowerCase())) {
        const command = collections.aliases.get(this.args[0].toLowerCase()) ?? this.args[0].toLowerCase();
        const info = collections.info.get(command);
        // TODO: room-specific prefix
        const prefix = htmlescape(process.env.PREFIX);
        html = `<h3><ins>mrmBot Help</ins></h3><h6><code>${prefix}${command}</code></h6>${info.description}`
        return { html: html }
      }
      if (help.categories[this.args[0].toLowerCase()]) {
        html = `<h2>mrmBot Help - ${htmlescape(this.args[0])}</h2><table><tr><th> Command </th><th> Description </th></tr>\n`
        for (const [command] of collections.commands) {
          if (collections.info.get(command).category != this.args[0].toLowerCase()) continue;
          const description = collections.info.get(command).description;
          html = html + `<tr><td> ${command} </td><td> ${description} </td></tr>\n`
        }
        html = html + "</table><br><sub>(mobile formatting will be fixed soon)</sub>"
        return { html: html }
      }
    }
    const prefix = htmlescape(process.env.PREFIX);
    html = `<h2>mrmBot Help - Categories</h2><table><tr><th> Category </th><th> Command </th></tr>`
    for (const category of Object.keys(help.categories)) {
      html = html + `<tr><td> ${category} </td><td> ${prefix}help ${category} </td></tr>`
    }
    html = html + "</table><br><sub>(mobile formatting will be fixed soon)</sub>"

    return { html: html }
    // return { html: "<h1>There are no mrmBot Docs Yet</h1>In the meantime, please refer to https://esmbot.net/help.html" };
  }
  
  static category = "general"
  static description = "Gets a list of commands";
  static aliases = ["commands"];
  static arguments = ["{command}"];
  static slashAllowed = false;
}

export default HelpCommand;
