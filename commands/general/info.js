import { readFileSync } from "fs";
const { version } = JSON.parse(readFileSync(new URL("../../package.json", import.meta.url)));
import Command from "../../classes/command.js";
import { getServers } from "../../utils/misc.js";

class InfoCommand extends Command {
  static category = "general"
  async run() {
    // await this.acknowledge();
    return { html: `<h1>mrmBot Info/Credits</h1>This instance is managed by <b>${process.env.OWNER.split(",")[0]}</b>.<br>v${version}-mrm (${process.env.GIT_REV})<br>Bot by <a href="https://essem.space"><font color="#FF0000"><i>Essem</i></font></a> and <a href="https://github.com/esmBot/esmBot/graphs/contributors"><i>other contributors</i></a>.<br>Fork by <a href="https://murm.gay"><font color="#18FF00"><b>Murm</b></font></a>.` };
  }

  static description = "Gets some info and credits about me";
  static aliases = ["botinfo", "credits"];
}

export default InfoCommand;