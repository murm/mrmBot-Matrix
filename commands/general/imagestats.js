import Command from "../../classes/command.js";
import { connections } from "../../utils/image.js";

class ImageStatsCommand extends Command {
  static category = "general"
  async run() {
    // await this.acknowledge();
    let desc = `The bot is currently connected to ${connections.size} image server(s).\n`
    let i = 0;
    for (const connection of connections.values()) {
      const count = await connection.getCount();
      if (!count) continue;
      desc = desc + `Server ${i++}\nRunning Jobs: ${count}`
    }
    return desc;
  }

  static description = "Gets some statistics about the image servers";
  static aliases = ["imgstat", "imstats", "imgstats", "imstat"];
}

export default ImageStatsCommand;
