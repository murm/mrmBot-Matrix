import database from "../../utils/database.js";
import Command from "../../classes/command.js";
import * as collections from "../../utils/collections.js";
import { htmlescape } from "../../utils/misc.js";

class CountCommand extends Command {
  async run() {
    const counts = await database.getCounts();
    if (this.args.length !== 0) {
      if (collections.commands.has(this.args[0].toLowerCase())) {
        let html;
        const command = collections.aliases.get(this.args[0].toLowerCase()) ?? this.args[0].toLowerCase();
        // TODO: room-specific prefix
        const prefix = htmlescape(process.env.PREFIX);
        let amount = counts[command]
        if (amount == 1) {
          amount = `<b>${amount}</b> time!`
        } else {
          amount = `<b>${amount}</b> times!`
        }
        html = `The command <code>${prefix}${command}</code> has been used ${amount}`
        return { html: html }
      }
      return "You need to specify a valid command to see its usage amount!"
    }
    return "You need to specify a command to see its usage amount!"
  }
  
  static category = "general"
  static description = "Gets how many times a command was used";
  static arguments = ["{mention/id}"];
  static aliases = ["counts"];
  static dbRequired = true;
}

export default CountCommand;