# <img src="https://gitdab.com/murm/mrmBot-matrix/raw/master/docs/assets/mrmbot.gif" width="128"> mrmBot-matrix

mrmBot-matrix is a free and open-source Matrix bot designed to entertain your server. It's made using [matrix-js-sdk](https://github.com/matrix-org/matrix-js-sdk) and comes with image and utility commands out of the box.

## Features
- Powerful, efficient, and performant image processing powered by [libvips](https://github.com/libvips/libvips)
- Lots of image manipulation and processing commands out of the box
- Handling of output images larger than 8MB via a local web server
- Optional WebSocket/HTTP-based external image API with load balancing
- Server tags system for saving/retrieving content
- Low RAM and CPU usage when idle
- Support for multiple database backends (PostgreSQL and SQLite backends included)
- [PM2](https://pm2.keymetrics.io)-based cluster/shard handling
- Flexible command handler allowing you to create new commands by adding script files

## Usage
At the present moment you need to self-host mrmBot-matrix if you wish to use it in your matrix rooms.

A command list for esmBot (which is the base for mrmBot-matrix) can be found [here](https://esmbot.net/help.html).

If you want to self-host the bot, a guide for esmBot (which is the base for mrmBot-matrix) can be found [here](https://docs.esmbot.net/setup).

## Credits
esmBot by [Essem](https://essem.space)
Icon by [pastarru](https://twitter.com/pastarru).
All images, sounds, and fonts are copyright of their respective owners.
